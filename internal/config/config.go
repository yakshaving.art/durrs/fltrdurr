package config

import (
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/filter"
)

func ParseByExtension(filename string) ([]filter.Filter, error) {
	switch strings.ToLower(path.Ext(filename)) {
	case ".yaml", ".yml":
		logrus.Debugf("Decoding yaml file %q", filename)
		y, err := loadYAMLConfiguration(filename)
		if err != nil {
			return nil, err
		}
		return y.ToFilters(), nil

	default:
		logrus.Debugf("Decoding toml file %q", filename)
		return decodeTomlFile(filename)
	}
}
