package filter

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"google.golang.org/api/gmail/v1"
)

func TestFilterTofltrdurr(t *testing.T) {
	testCases := map[string]struct {
		orig     Filter
		expected gmail.Filter
	}{
		"archive": {
			orig: Filter{
				Archive: true,
				Label:   "Mailing Lists/coreos-dev",
				Query:   "list:coreos-dev@googlegroups.com",
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{},
					RemoveLabelIds: []string{"INBOX"},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "list:coreos-dev@googlegroups.com",
				},
			},
		},
		"archive with OR": {
			orig: Filter{
				Archive: true,
				Label:   "Mailing Lists/xdg-apps",
				QueryOr: []string{"list:xdg-app@lists.freedesktop.org", "list:flatpak@lists.freedesktop.org"},
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{},
					RemoveLabelIds: []string{"INBOX"},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "(list:xdg-app@lists.freedesktop.org) OR (list:flatpak@lists.freedesktop.org)",
				},
			},
		},
		"delete": {
			orig: Filter{
				Delete:  true,
				QueryOr: []string{"to:plans@tripit.com", "to:receipts@expensify.com"},
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{"TRASH"},
					RemoveLabelIds: []string{},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "(to:plans@tripit.com) OR (to:receipts@expensify.com)",
				},
			},
		},
		"archive and read": {
			orig: Filter{
				Archive: true,
				QueryOr: []string{"to:plans@tripit.com", "to:receipts@expensify.com"},
				Read:    true,
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{},
					RemoveLabelIds: []string{"INBOX", "UNREAD"},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "(to:plans@tripit.com) OR (to:receipts@expensify.com)",
				},
			},
		},
		"to me": {
			orig: Filter{
				Archive: true,
				QueryOr: []string{"to:plans@tripit.com", "to:receipts@expensify.com"},
				Read:    true,
				ToMe:    true,
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{},
					RemoveLabelIds: []string{"INBOX", "UNREAD"},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "(to:plans@tripit.com) OR (to:receipts@expensify.com)",
					To:    "me",
				},
			},
		},
		"forward": {
			orig: Filter{
				ForwardTo: "someone@somewhere.net",
				QueryOr:   []string{"to:plans@tripit.com", "to:receipts@expensify.com"},
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					AddLabelIds:    []string{},
					Forward:        "someone@somewhere.net",
					RemoveLabelIds: []string{},
				},
				Criteria: &gmail.FilterCriteria{
					Query: "(to:plans@tripit.com) OR (to:receipts@expensify.com)",
				},
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			filter, err := tc.orig.ToGmailFilter()
			if err != nil {
				t.Fatal(err)
			}

			if diff := cmp.Diff(tc.expected, filter); len(diff) > 1 {
				t.Fatalf("got diff: %s", diff)
			}
		})
	}
}

func TestArchiveUnlessToMe(t *testing.T) {
	testCases := map[string]struct {
		orig     gmail.Filter
		expected gmail.Filter
	}{
		"single filter": {
			orig: gmail.Filter{
				Action: &gmail.FilterAction{},
				Criteria: &gmail.FilterCriteria{
					Query: "list:coreos-dev@googlegroups.com",
				},
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					RemoveLabelIds: []string{"INBOX"},
				},
				Criteria: &gmail.FilterCriteria{
					NegatedQuery: "to:me",
					Query:        "list:coreos-dev@googlegroups.com",
					To:           "",
				},
			},
		},
		"filter with OR": {
			orig: gmail.Filter{
				Action: &gmail.FilterAction{},
				Criteria: &gmail.FilterCriteria{
					Query: "(list:xdg-app@lists.freedesktop.org) OR (list:flatpak@lists.freedesktop.org)",
				},
			},
			expected: gmail.Filter{
				Action: &gmail.FilterAction{
					RemoveLabelIds: []string{"INBOX"},
				},
				Criteria: &gmail.FilterCriteria{
					NegatedQuery: "to:me",
					Query:        "(list:xdg-app@lists.freedesktop.org) OR (list:flatpak@lists.freedesktop.org)",
					To:           "",
				},
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			filter := ArchiveUnlessToMe(tc.orig)
			if diff := cmp.Diff(tc.expected, filter); len(diff) > 1 {
				t.Fatalf("got diff: %s", diff)
			}
		})
	}
}
