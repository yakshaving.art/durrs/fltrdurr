package label

import (
	"strings"

	"google.golang.org/api/gmail/v1"
)

type LabelMap map[string]string

// GetLabelMap gets the labels for the user and map its name to its ID.
func GetLabelMap(labelsList *gmail.ListLabelsResponse) (LabelMap, error) {
	labels := LabelMap{}
	for _, label := range labelsList.Labels {
		labels[strings.ToLower(label.Name)] = label.Id
	}
	return labels, nil
}

// GetLabelMapOnID gets the labels for the user and map its name to its ID.
func GetLabelMapOnID(labelsList *gmail.ListLabelsResponse) (LabelMap, error) {
	labels := LabelMap{}
	for _, label := range labelsList.Labels {
		labels[label.Id] = label.Name
	}

	return labels, nil
}
