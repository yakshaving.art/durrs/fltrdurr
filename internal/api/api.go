package api

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/auth"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/config"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/filter"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/label"
	"golang.org/x/net/context"
	"google.golang.org/api/gmail/v1"
)

const (
	gmailUser = "me"
)

// API Errors
var (
	ErrInvalidOldEmailsModifyAction = fmt.Errorf("Invalid old email modification criteria")
)

// GmailClient is a client of the filters gmail API
type GmailClient interface {
	ApplyLabelsToOldEmails(f gmail.Filter) error
	CreateFilter(*gmail.Filter) error
	CreateLabel(*gmail.Label) (string, error)
	DeleteFilter(string) error
	ListFilters() (*gmail.ListFiltersResponse, error)
	ListLabels() (*gmail.ListLabelsResponse, error)
}

// Client is an api client
type Client struct {
	GmailClient GmailClient
	Labels      *label.LabelMap
}

func GetClient(ctx context.Context, auth *auth.Authenticator, dryrun bool) (Client, error) {
	// Get the client from the config.
	client := auth.Config.Client(ctx, auth.Token)

	// Create the service for the Gmail client.
	serviceClient, err := gmail.New(client) // TODO: Switch to NewService()
	if err != nil {
		return Client{}, fmt.Errorf("creating Gmail client failed: %v", err)
	}

	gc := gmailClient{api: serviceClient}

	if dryrun {
		logrus.Debugf("Creating a DRYRUN client")
		return Client{
			GmailClient: DryrunClient{
				Inner: gc,
			},
			Labels: &label.LabelMap{},
		}, nil
	} else {
		logrus.Debugf("Creating a default api client (it will make actual changes)")
		return Client{
			GmailClient: gc,
			Labels:      &label.LabelMap{},
		}, nil
	}
}

func (c Client) CreateFilter(f filter.Filter, applyLabelsToOldEmails bool) error {
	convertedFilter, err := f.ToGmailFilter()
	if err != nil {
		return err
	}

	// Create the label if it does not exist
	if len(f.Label) > 0 {
		labelID, err := c.CreateLabelIfDoesNotExist(f.Label, c.Labels)
		if err != nil {
			return err
		}
		convertedFilter.Action.AddLabelIds = append(convertedFilter.Action.AddLabelIds, labelID)
	} else {
		logrus.Debugf("label for filter %#v already exists: skipping creation", f)
	}

	filters := []gmail.Filter{
		convertedFilter,
	}

	// If we need to archive unless to them, then add the additional filter
	if f.ArchiveUnlessToMe {
		// Append the extra filter
		filters = append(filters, filter.ArchiveUnlessToMe(convertedFilter))
	}
	logrus.Debugf("Resulting filters: %+v", filters)

	// Create the filters
	for _, fltr := range filters {
		logrus.Debugf("Creating a filter. Details:")
		logrus.Debugf("Action: %+v", fltr.Action)
		logrus.Debugf("Criteria: %+v\n", fltr.Criteria)

		if err := c.GmailClient.CreateFilter(&fltr); err != nil {
			return fmt.Errorf("creating filter [%#v] failed: %w", fltr, err)
		}

		if applyLabelsToOldEmails {
			if err := c.GmailClient.ApplyLabelsToOldEmails(fltr); err != nil {
				return fmt.Errorf("failed to apply filter to old emails: %w", err)
			}
		}
	}
	return nil
}

func (c Client) GetExistingFilters() ([]filter.Filter, error) {
	fltrdurr, err := c.GmailClient.ListFilters()
	if err != nil {
		return nil, err
	}

	labelsList, err := c.GmailClient.ListLabels()
	if err != nil {
		return nil, fmt.Errorf("listing labels failed: %v", err)
	}

	labels, err := label.GetLabelMapOnID(labelsList)
	if err != nil {
		return nil, err
	}

	var filters []filter.Filter

	for _, gmailFilter := range fltrdurr.Filter {
		var f filter.Filter

		if gmailFilter.Criteria.Query > "" {
			f.Query = gmailFilter.Criteria.Query

			if gmailFilter.Criteria.To == "me" {
				f.ToMe = true
			}

			if len(gmailFilter.Action.AddLabelIds) > 0 {
				labelID := gmailFilter.Action.AddLabelIds[0]
				if labelID == "TRASH" {
					f.Delete = true
				} else {
					labelName, ok := labels[labelID]
					if ok {
						f.Label = labelName
					}
				}
			}

			if len(gmailFilter.Action.RemoveLabelIds) > 0 {
				for _, labelID := range gmailFilter.Action.RemoveLabelIds {
					if labelID == "UNREAD" {
						f.Read = true
					} else if labelID == "INBOX" {
						if gmailFilter.Criteria.NegatedQuery == "to:me" {
							f.ArchiveUnlessToMe = true
						} else {
							f.Archive = true
						}
					}
				}
			}
		}

		filters = append(filters, f)
	}

	return filters, nil
}

func (c Client) GetLabels() (*label.LabelMap, error) {
	rawLabels, err := c.GmailClient.ListLabels()
	if err != nil {
		return nil, fmt.Errorf("listing labels failed: %v", err)
	}
	labelMap, err := label.GetLabelMap(rawLabels)
	if err != nil {
		return nil, fmt.Errorf("mapping labels failed: %v", err)
	}
	logrus.Debugf("LabelMap: %+v", labelMap)
	return &labelMap, nil
}

func (c Client) CreateLabelIfDoesNotExist(name string, m *label.LabelMap) (string, error) {
	// Dereference the pointer so we can index.
	labels := *m

	// Try to find the label.
	id, ok := labels[strings.ToLower(name)]
	if ok {
		// We found the label.
		return id, nil
	}

	// Create the label if it does not exist.
	labelID, err := c.GmailClient.CreateLabel(&gmail.Label{Name: name})
	if err != nil {
		return "", fmt.Errorf("creating label %s failed: %v", name, err)
	}
	logrus.Infof("Created label: %s", name)

	// Update our label map.
	labels[strings.ToLower(name)] = labelID
	m = &labels
	return labelID, nil
}

func (c Client) DeleteExistingFilters() error {
	logrus.Debugf("Deleting old filters")

	// Get current filters for the user.
	l, err := c.GmailClient.ListFilters()
	if err != nil {
		return fmt.Errorf("listing filters failed: %v", err)
	}

	// Iterate over the filters.
	for _, f := range l.Filter {
		// Delete the filter.
		if err := c.GmailClient.DeleteFilter(f.Id); err != nil {
			return fmt.Errorf("deleting filter id %s failed: %w", f.Id, err)
		}
	}

	logrus.Infof("Old filters deleted")
	return nil
}

func (c Client) ExportExistingFilters(file string) error {
	fmt.Print("exporting existing filters...\n")

	filters, err := c.GetExistingFilters()
	if err != nil {
		return fmt.Errorf("error downloading existing filters: %v", err)
	}

	var ff filter.Filterfile
	for _, f := range filters {
		// We could get duplicate filters, so it's best to remove them.
		existingFilter := filter.FindExistingFilter(ff.Filter, f.Query)

		// Since we can't return nil on a struct or compary it to something empty,
		// check if the query exists. If not then consider it not found.
		if existingFilter.Query != "" {
			// Duplicate filters can only exist if the ArchiveUnlessToMe is set.
			// So we can simply reset everything and just set the ArchiveUnlessToMe flag to true.
			existingFilter.Archive = false
			existingFilter.Delete = false
			existingFilter.ToMe = false
			existingFilter.ArchiveUnlessToMe = true
		} else {
			ff.Filter = append(ff.Filter, f)
		}
	}

	return config.WriteFiltersToFile(ff, file)
}
