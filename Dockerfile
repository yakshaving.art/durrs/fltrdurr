FROM alpine:3

RUN apk --no-cache add ca-certificates bash

COPY fltrdurr /usr/bin/fltrdurr

ENTRYPOINT [ "/usr/bin/fltrdurr" ]
CMD [ "--help" ]
