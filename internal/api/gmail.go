package api

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/api/gmail/v1"
)

// gmailClient is an implementation of a GmailClient
type gmailClient struct {
	api *gmail.Service
}

// List lists filters
func (g gmailClient) ListFilters() (*gmail.ListFiltersResponse, error) {
	return g.api.Users.Settings.Filters.List(gmailUser).Do()
}

// Create creates a new filter
func (g gmailClient) CreateFilter(f *gmail.Filter) error {
	if _, err := g.api.Users.Settings.Filters.Create(gmailUser, f).Do(); err != nil {
		return err
	}
	return nil
}

// Delete deletes a filter
func (g gmailClient) DeleteFilter(id string) error {
	return g.api.Users.Settings.Filters.Delete(gmailUser, id).Do()
}

// ListLabels lists the labels
func (g gmailClient) ListLabels() (*gmail.ListLabelsResponse, error) {
	return g.api.Users.Labels.List(gmailUser).Do()

}

// CreateLabel creates a new label
func (g gmailClient) CreateLabel(lbl *gmail.Label) (string, error) {
	l, err := g.api.Users.Labels.Create(gmailUser, lbl).Do()
	if err != nil {
		return "", err
	}
	return l.Id, nil
}

func (g gmailClient) ApplyLabelsToOldEmails(f gmail.Filter) error {
	if f.Criteria.Query == "" {
		return fmt.Errorf("Empty criteria cannot be applied to old messages: %w", ErrInvalidOldEmailsModifyAction)
	}
	if len(f.Action.AddLabelIds) == 0 {
		return fmt.Errorf("No labels to be applied to old messages: %w", ErrInvalidOldEmailsModifyAction)
	}

	var totalEmails int

	ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Minute)
	defer cancel()

	applyLabels := func(ids []string) error {
		logrus.Debugf("Applying labels %#v to %d emails...", f.Action.AddLabelIds, len(ids))
		if err := g.api.Users.Messages.BatchModify(gmailUser, &gmail.BatchModifyMessagesRequest{
			AddLabelIds:    f.Action.AddLabelIds,
			Ids:            ids,
			RemoveLabelIds: f.Action.RemoveLabelIds,
		}).Do(); err != nil {
			return fmt.Errorf("failed to batch modify %d messages to apply %s and remove %s labels: %w",
				len(ids),
				strings.Join(f.Action.AddLabelIds, ", "),
				strings.Join(f.Action.RemoveLabelIds, ", "),
				err)
		}
		return nil
	}

	err := g.api.Users.Messages.List(gmailUser).Q(f.Criteria.Query).Pages(ctx, func(r *gmail.ListMessagesResponse) error {
		ids := make([]string, 0)
		for _, i := range r.Messages {
			ids = append(ids, i.Id)
			totalEmails++

			if len(ids) == 1000 {
				err := applyLabels(ids)
				if err != nil {
					return err
				}
				ids = make([]string, 0)
			}
		}
		if len(ids) > 0 {
			return applyLabels(ids)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("Failed to apply labels %#v to all email messages that match the query: %w", f.Action.AddLabelIds, err)
	}
	logrus.Infof("Labels %q applied and %s removed to %d emails in total",
		strings.Join(f.Action.AddLabelIds, ", "),
		strings.Join(f.Action.RemoveLabelIds, ", "),
		totalEmails)
	return nil
}
