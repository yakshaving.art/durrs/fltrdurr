package auth

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
)

const (
	authMessage = `Go to the following link in your browser to authorise this application: `
)

// Authenticator contains all that's needed to authenticate against OAuth2
type Authenticator struct {
	Config *oauth2.Config
	State  string
	Token  *oauth2.Token
}

func Authenticate(credentialsPath string, tokenPath string, noAuth bool) (*Authenticator, error) {
	a := &Authenticator{
		State: generateOauthState(),
	}
	err := a.getCredentialsFromFile(credentialsPath)
	if err != nil {
		return nil, fmt.Errorf("creating config from credentials: %w", err)
	}
	err = a.getTokenFromFile(tokenPath)
	if err == nil {
		logrus.Debugf("token found: returning to the main function")
		return a, nil
	}

	if noAuth {
		logrus.Fatalln("noauth set and token not found: exiting")
	}
	logrus.Debugf("token not found (%s): getting one", err)
	localSrv := newOauth2Server(a.State)
	addr, err := localSrv.Start()
	if err != nil {
		return nil, fmt.Errorf("starting local server: %w", err)
	}
	defer localSrv.Close()

	fmt.Println(authMessage, a.authURL("http://"+addr))
	authCode := localSrv.WaitForCode()
	if err := a.saveToken(tokenPath, authCode); err != nil {
		return nil, fmt.Errorf("error writing token: %w", err)
	}
	return a, nil
}

// GetTokenFromWeb requests a token from the web, then returns the retrieved token.
func GetTokenFromWeb(ctx context.Context, config *oauth2.Config) (*oauth2.Token, error) {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		return nil, fmt.Errorf("unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(ctx, authCode)
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve token from web: %v", err)
	}

	return tok, nil
}

// AuthURL returns the URL the user has to visit to authorize the
// application and obtain an auth code.
func (a *Authenticator) authURL(redirectURL string) string {
	a.Config.RedirectURL = redirectURL
	return a.Config.AuthCodeURL(a.State, oauth2.AccessTypeOffline)
}

func (a *Authenticator) getCredentialsFromFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("error opening credentials: %w", err)
	}
	credBytes, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("error reading credentials: %w", err)
	}
	a.Config, err = google.ConfigFromJSON(credBytes,
		gmail.GmailSettingsBasicScope,
		gmail.GmailLabelsScope,
	)
	if err != nil {
		return fmt.Errorf("error parsing credentials: %w", err)
	}
	return nil
}

func (a *Authenticator) getTokenFromFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("failed to open token file: %w", err)
	}
	defer f.Close()
	token := new(oauth2.Token)
	err = json.NewDecoder(f).Decode(token)
	if err != nil {
		return fmt.Errorf("failed to decode token: %w", err)
	}
	a.Token = token
	return nil
}

func (a *Authenticator) saveToken(path, authCode string) (err error) {
	logrus.Infof("saving credential file to %s\n", path)
	f, e := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if e != nil {
		return fmt.Errorf("creating token file: %w", e)
	}
	defer func() {
		e = f.Close()
		// Do not hide more important errors.
		if err == nil {
			err = e
		}
	}()

	token, err := a.Config.Exchange(context.Background(), authCode)
	if err != nil {
		return fmt.Errorf("unable to retrieve token from web: %w", err)
	}
	a.Token = token
	return json.NewEncoder(f).Encode(token)
}

// SaveTokenToFile saves a token to a file path.
func SaveTokenToFile(path string, token *oauth2.Token) error {
	logrus.Infof("Saving credential file to: %s", path)

	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return fmt.Errorf("unable to cache oauth token: %v", err)
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(token)
}

func generateOauthState() string {
	b := make([]byte, 128)
	if _, err := rand.Read(b); err != nil {
		// We can't really afford errors in secure random number generation.
		panic(err)
	}
	state := base64.URLEncoding.EncodeToString(b)
	return state
}
