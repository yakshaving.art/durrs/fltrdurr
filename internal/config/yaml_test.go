package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/filter"
)

func TestParsingYamlsLabels(t *testing.T) {
	a := assert.New(t)
	c, err := loadYAMLConfiguration("fixtures/flat.yml")
	a.NoError(err)

	a.Equal([]string{"list:other-stuff"}, c.Delete)

	want := map[string]filter.Filter{
		"MyLabelName": {
			Archive:           true,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "MyLabelName",
			Query:             "",
			QueryOr:           []string{"list:stuff"},
			Read:              false,
			ToMe:              false,
		},
		"MyOtherLabelName/MySubLabel": {
			Archive:           true,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "MyOtherLabelName/MySubLabel",
			Query:             "",
			QueryOr: []string{
				"subject:Hello, is it me you're looking for?",
				"subject:I can see it in your eyes",
				"I can see it in your smile",
				"You're all I've ever wanted"},
			Read: false,
			ToMe: false,
		},
		"MyOtherLabelName/MySubLabel/SubLabelThree": {
			Archive:           true,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "MyOtherLabelName/MySubLabel/SubLabelThree",
			Query:             "",
			QueryOr:           []string{"to:myself"},
			Read:              false,
			ToMe:              false,
		},
		"SomeLabelName/AnotherSubLabel": {
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "SomeLabelName/AnotherSubLabel",
			Query:             "",
			QueryOr:           []string{"free text"},
			Read:              false,
			ToMe:              false,
		},
		"SomeLabelName/AnotherSubLabel/ThisIsVeryDeep": {
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "SomeLabelName/AnotherSubLabel/ThisIsVeryDeep",
			Query:             "",
			QueryOr:           []string{"to:hellothere"},
			Read:              false,
			ToMe:              false,
		},
		"MoarStuff": {
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             "MoarStuff",
			Query:             "",
			QueryOr:           []string{"list:moarstuff"},
			Read:              false,
			ToMe:              false,
		},
		"": {
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            true,
			ForwardTo:         "",
			Label:             "",
			Query:             "list:other-stuff",
			QueryOr:           []string{},
			Read:              false,
			ToMe:              false,
		},
	}
	got := c.ToFilters()
	for _, f := range got {
		a.Equal(f, want[f.Label])
	}
	a.Equal(len(want), len(got))
}
