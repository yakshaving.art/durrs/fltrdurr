package filter

import (
	"errors"
	"fmt"
	"strings"

	"google.golang.org/api/gmail/v1"
)

// Filterfile defines a set of filter objects.
type Filterfile struct {
	Filter []Filter
}

// Filter defines a filter object.
type Filter struct {
	Archive           bool
	ArchiveUnlessToMe bool
	Delete            bool
	ForwardTo         string
	Label             string
	Query             string
	QueryOr           []string
	Read              bool
	ToMe              bool
}

// ToGmailFilter converts the an action into one or more gmail filters.
func (f Filter) ToGmailFilter() (gmail.Filter, error) {
	if len(f.Query) > 0 && len(f.QueryOr) > 0 {
		return gmail.Filter{}, errors.New("cannot have both a query and a queryOr")
	}

	if len(f.QueryOr) > 0 {
		// Create the OR query
		inner := strings.Join(f.QueryOr, ") OR (")
		f.Query = fmt.Sprintf("(%s)", inner)
	}

	if len(f.Query) < 1 {
		return gmail.Filter{}, errors.New("query or queryOr cannot be empty")
	}

	action := gmail.FilterAction{
		AddLabelIds:    []string{},
		RemoveLabelIds: []string{},
	}

	if f.Archive && !f.ArchiveUnlessToMe {
		action.RemoveLabelIds = append(action.RemoveLabelIds, "INBOX")
	}

	if f.Read {
		action.RemoveLabelIds = append(action.RemoveLabelIds, "UNREAD")
	}

	if f.Delete {
		action.AddLabelIds = append(action.AddLabelIds, "TRASH")
	}

	if len(f.ForwardTo) > 0 {
		action.Forward = f.ForwardTo
	}

	criteria := gmail.FilterCriteria{Query: f.Query}

	if f.ToMe || f.ArchiveUnlessToMe {
		criteria.To = "me"
	}

	return gmail.Filter{
		Action:   &action,
		Criteria: &criteria,
	}, nil
}

func ArchiveUnlessToMe(fltr gmail.Filter) gmail.Filter {
	fltr.Criteria = &gmail.FilterCriteria{
		NegatedQuery: "to:me",
		Query:        fltr.Criteria.Query,
		To:           "",
	}

	// Archive it.
	fltr.Action.RemoveLabelIds = append(fltr.Action.RemoveLabelIds, "INBOX")
	return fltr
}

func FindExistingFilter(filters []Filter, query string) Filter {
	for _, f := range filters {
		if f.Query == query {
			return f
		}
	}

	return Filter{}
}
