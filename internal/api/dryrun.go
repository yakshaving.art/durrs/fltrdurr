package api

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"google.golang.org/api/gmail/v1"
)

// DryrunClient is a read-only client and an implementationof GmailClient
type DryrunClient struct {
	Inner gmailClient
}

// ListFilters fetches a list of filters
func (d DryrunClient) ListFilters() (*gmail.ListFiltersResponse, error) {
	return d.Inner.ListFilters()
}

// CreateFilter pretends to create a filter
func (d DryrunClient) CreateFilter(fltr *gmail.Filter) error {
	logrus.Infof("SKIPPING creating a filter. Details:")
	logrus.Infof("Action: %+v", fltr.Action)
	logrus.Infof("Criteria: %+v", fltr.Criteria)
	return nil
}

// DeleteFilter pretends to delete a filter
func (d DryrunClient) DeleteFilter(f string) error {
	logrus.Infof("SKIPPING deleting a filter %#v", f)
	return nil
}

// ListLabels fetches a list of labels
func (d DryrunClient) ListLabels() (*gmail.ListLabelsResponse, error) {
	return d.Inner.ListLabels()
}

// CreateLabel pretends to create a label
func (d DryrunClient) CreateLabel(l *gmail.Label) (string, error) {
	logrus.Infof("SKIPPING creating a label %#v", l)
	return "1", nil

}

func (d DryrunClient) ApplyLabelsToOldEmails(f gmail.Filter) error {
	if _, err := d.Inner.api.Users.Messages.List(gmailUser).Q(f.Criteria.Query).MaxResults(1).Do(); err != nil {
		return fmt.Errorf("We don't have enough permissions to query all messages through the API: %w", err)
	}
	logrus.Infof("SKIPPING applying a filter %#v to all old emails, but we can read them", f)
	return nil
}
