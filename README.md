# fltrdurr

A tool to sync Gmail filters from a config file to your account.

> **NOTE:** This makes it so the single configuration file is the only way to
   add filters to your account, meaning if you add a filter via the UI and do not
   also add it in your config file, the next time you run this tool on your
   outdated config, the filter you added _only_ in the UI will be deleted.

## Contents
- [Usage](#usage)
- [Configuration](#configuration)
- [Setup authentication](#setup-authentication)

## Usage

```console
fltrdurr <flags> <manifest file>
```

Flags:

```console
  -a                apply filters to all emails that match (default: false)
  --apply           apply labels to new emails (default: false)
  -d, --debug       enable debug logging (default: false)
  --dryrun          enable dryrun mode (default: false)
  -e, --export      export existing filters (default: false)
  -f, --creds-file  Gmail credential file (default: credentials.json)
  --noauth          fail unless a token is already available (default: false)
  -t, --token-file  Gmail oauth token file (default: token.json)
```

## Configuration

Here you can find examples about how to write your filters manifest.

### Example TOML Filter File

<details>
<summary>Show</summary>
<br>

```toml
[[filter]]
query = "to:your_activity@noreply.github.com"
archive = true
read = true

[[filter]]
query = "from:notifications@github.com LGTM"
label = "github/LGTM"

[[filter]]
query = """
(-to:team_mention@noreply.github.com \
(from:(notifications@github.com) AND (@jfrazelle OR @jessfraz OR to:mention@noreply.github.com OR to:author@noreply.github.com OR to:assign@noreply.github.com)))
"""
label = "github/mentions"

[[filter]]
query = """
to:team_mention@noreply.github.com \
-to:mention@noreply.github.com \
-to:author@noreply.github.com \
-to:assign@noreply.github.com
"""
label = "github/team-mention"

[[filter]]
query = """
from:notifications@github.com \
-to:team_mention@noreply.github.com \
-to:mention@noreply.github.com \
-to:author@noreply.github.com \
-to:assign@noreply.github.com
"""
archive = true

[[filter]]
query = "(from:me AND to:reply@reply.github.com)"
label = "github/mentions"

[[filter]]
query = "(from:notifications@github.com)"
label = "github"

[[filter]]
queryOr = [
"to:plans@tripit.com",
"to:receipts@concur.com",
"to:plans@concur.com",
"to:receipts@expensify.com"
]
delete = true

[[filter]]
queryOr = [
"from:notifications@docker.com",
"from:noreply@github.com",
"from:builds@travis-ci.org"
]
label = "to-be-deleted"

[[filter]]
query = "drive-shares-noreply@google.com OR (subject:\"Invitation to comment\" AND from:me ) OR from:(*@docs.google.com)"
label = "to-be-deleted"

[[filter]]
query = "(from:(-me) {filename:vcs filename:ics} has:attachment) OR (subject:(\"invitation\" OR \"accepted\" OR \"tentatively accepted\" OR \"rejected\" OR \"updated\" OR \"canceled event\" OR \"declined\") when where calendar who organizer)"
label = "to-be-deleted"

[[filter]]
query = "list:coreos-dev@googlegroups.com"
label = "Mailing Lists/coreos-dev"
archiveUnlessToMe = true

[[filter]]
queryOr = [
"list:xdg-app@lists.freedesktop.org",
"list:flatpak@lists.freedesktop.org"
]
label = "Mailing Lists/xdg-apps"
archiveUnlessToMe = true
```
</details>

### Example YAML configuration file

Archive labels an incoming email and immediately archive it, so it won't show up in your inbox.

Label will labels an incoming email and leaves it in your inbox, so you'll get notified.

Delete sends the matching incoming email straight to the bin.

<details>
<summary>Show</summary>
<br>

```yaml
---
archive:
  MyLabelName:
    - "list:stuff"
  MyOtherLabelName/MySubLabel:
    - "subject:Hello, is it me you're looking for?"
    - "subject:I can see it in your eyes"
    - "I can see it in your smile"
    - "You're all I've ever wanted"
  MyOtherLabelName/MySubLabel/SubLabelThree:
    - "to:myself"

label:
  SomeLabelName/AnotherSubLabel:
    - "free text"
  SomeLabelName/AnotherSubLabel/ThisIsVeryDeep:
    - "to:hellothere"
  MoarStuff:
    - "list:moarstuff"

delete:
  - "list:other-stuff"
```
</details>

## Setup authentication

### Obtaining the OAuth credentials

* Go to https://console.cloud.google.com/apis.
* Choose OAuth consent screen.
* Set it up like this:
  * App name: GMail Filters
  * User support email: your email
  * Developer contact information: your email
* Go to credentials.
* Create a new OAuth client ID.
  * Application type: Desktop app
  * Name: fltrdurr
* Save and download from the list (last icon on the right). Call the file `credentials.json`. This is what your `--creds-file` should point to.

### Obtaining the API token
* Open the terminal and `cd` to the directory where you downloaded the credentials file.
* Run `fltrdurr`.
* Follow the link and the instructions on your browser.
* `token.json` is what your `--token-file` should point to.
