package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/genuinetools/pkg/cli"
	"github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/api"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/auth"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/config"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/version"
)

var (
	client           api.Client
	applyToOldEmails bool
	noAuth           bool
	credsPath        string
	debug            bool
	dryrun           bool
	export           bool
	tokenPath        string
)

func main() {
	// Create a new cli program.
	p := cli.NewProgram()
	p.Name = "fltrdurr"
	p.Description = "A tool to sync Gmail filters from a config file to your account"
	// Set the GitCommit and Version.
	p.GitCommit = version.GITCOMMIT
	p.Version = version.VERSION

	// Setup the global flags.
	p.FlagSet = flag.NewFlagSet("fltrdurr", flag.ExitOnError)
	p.FlagSet.BoolVar(&applyToOldEmails, "a", false, "apply filters to all emails that match")
	p.FlagSet.BoolVar(&applyToOldEmails, "apply", false, "apply labels to new emails")
	p.FlagSet.BoolVar(&debug, "d", false, "enable debug logging")
	p.FlagSet.BoolVar(&debug, "debug", false, "enable debug logging")
	p.FlagSet.BoolVar(&dryrun, "dryrun", false, "enable dryrun mode")
	p.FlagSet.BoolVar(&export, "e", false, "export existing filters")
	p.FlagSet.BoolVar(&export, "export", false, "export existing filters")
	p.FlagSet.BoolVar(&noAuth, "noauth", false, "fail unless a token is already available")
	p.FlagSet.StringVar(&credsPath, "creds-file", "credentials.json", "Gmail credential file")
	p.FlagSet.StringVar(&credsPath, "f", "credentials.json", "Gmail credential file")
	p.FlagSet.StringVar(&tokenPath, "t", "token.json", "Gmail oauth token file")
	p.FlagSet.StringVar(&tokenPath, "token-file", "token.json", "Gmail oauth token file")

	// TODO: get credentials and token contents from environment variable

	// Set the before function.
	p.Before = func(ctx context.Context) error {
		// Set the log level.
		if debug {
			logrus.SetLevel(logrus.DebugLevel)
		}
		logrus.Debugf("Credentials path: %s\nToken path: %s\n", credsPath, tokenPath)
		authenticator, err := auth.Authenticate(credsPath, tokenPath, noAuth)
		if err != nil {
			logrus.Fatalf("failed to authenticate: %s", err)
		}

		// Get the client from the config.
		client, err = api.GetClient(ctx, authenticator, dryrun)
		if err != nil {
			return fmt.Errorf("creating client failed: %v", err)
		}

		// Load the existing labels
		client.Labels, err = client.GetLabels()
		if err != nil {
			return fmt.Errorf("failed to load labels: %s", err)
		}

		return nil
	}

	p.Action = func(ctx context.Context, args []string) error {
		if len(args) < 1 {
			return errors.New("must pass a path to a gmail filter configuration file")
		}

		// On ^C, or SIGTERM handle exit.
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		signal.Notify(c, syscall.SIGTERM)
		go func() {
			for sig := range c {
				logrus.Infof("Received %s, exiting.", sig.String())
				os.Exit(0)
			}
		}()

		if export {
			return client.ExportExistingFilters(args[0])
		}

		logrus.Infof("Decoding filters from file %s", args[0])
		filters, err := config.ParseByExtension(args[0])
		if err != nil {
			return err
		}

		logrus.Infof("Filters decoded into a list of %d", len(filters))
		// Delete our existing filters.
		if err := client.DeleteExistingFilters(); err != nil {
			return err
		}

		// Convert our filters into gmail filters and add them.
		logrus.Infof("Updating %d filters, this might take a bit...", len(filters))
		for _, f := range filters {
			if err := client.CreateFilter(f, applyToOldEmails); err != nil {
				// TODO: Maybe we want to continue to avoid half-baked deploys?
				return err
			}
		}

		logrus.Infof("Successfully updated %d filters", len(filters))

		return nil
	}

	// Run our program.
	p.Run()
}
