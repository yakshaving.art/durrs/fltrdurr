package config

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/BurntSushi/toml"
	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/filter"
)

func decodeTomlFile(file string) ([]filter.Filter, error) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("reading filter file %s failed: %v", file, err)
	}

	var ff filter.Filterfile
	if _, err := toml.Decode(string(b), &ff); err != nil {
		return nil, fmt.Errorf("decoding toml failed: %v", err)
	}

	return ff.Filter, nil
}

func WriteFiltersToFile(ff filter.Filterfile, file string) error {
	exportFile, err := os.Create(file)
	if err != nil {
		return fmt.Errorf("error exporting filters: %v", err)
	}

	writer := bufio.NewWriter(exportFile)
	encoder := toml.NewEncoder(writer)
	encoder.Indent = ""

	if err := encoder.Encode(ff); err != nil {
		return fmt.Errorf("error writing file: %v", err)
	}

	fmt.Printf("Exported %d filters\n", len(ff.Filter))

	return nil
}
