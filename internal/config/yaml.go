package config

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/yakshaving.art/durrs/fltrdurr/internal/filter"
	"gopkg.in/yaml.v2"
)

// YamlConfig is a configuration of labels from a yaml file
type YamlConfig struct {
	Archive map[string][]string `yaml:"archive"`
	Delete  []string            `yaml:"delete"`
	Label   map[string][]string `yaml:"label"`
}

// LoadYAMLConfiguration loads a YAML configuration
func loadYAMLConfiguration(file string) (YamlConfig, error) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return YamlConfig{}, fmt.Errorf("reading filter file %s failed: %v", file, err)
	}

	var c YamlConfig
	err = yaml.Unmarshal(b, &c)
	if err != nil {
		return YamlConfig{}, fmt.Errorf("failed to parse yaml %s: %w", file, err)
	}
	return c, nil
}

// ToFilters returns the list of filters from the yaml configuration
func (y YamlConfig) ToFilters() []filter.Filter {
	filters := []filter.Filter{}

	for label, queries := range y.Archive {
		filters = append(filters, filter.Filter{
			Archive:           true,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             label,
			Query:             "",
			QueryOr:           queries,
			Read:              false,
			ToMe:              false,
		})
	}

	for label, queries := range y.Label {
		filters = append(filters, filter.Filter{
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            false,
			ForwardTo:         "",
			Label:             label,
			Query:             "",
			QueryOr:           queries,
			Read:              false,
			ToMe:              false,
		})
	}

	for _, query := range y.Delete {
		filters = append(filters, filter.Filter{
			Archive:           false,
			ArchiveUnlessToMe: false,
			Delete:            true,
			ForwardTo:         "",
			Label:             "",
			Query:             query,
			QueryOr:           []string{},
			Read:              false,
			ToMe:              false,
		})
	}

	return filters
}
